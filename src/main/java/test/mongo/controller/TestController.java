package test.mongo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import test.mongo.entity.CustomCollection;
import test.mongo.pojo.Address;
import test.mongo.service.MongoService;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private MongoService mongoService;

    @GetMapping("/")
    public String getString() {
        return "test String";
    }


    @GetMapping("/create")
    public String craeteEntry() {
        double random = Math.random();
        mongoService.createEntry(new CustomCollection(
            random + "name", random + "contact", new Address(random + "street", random + "city")));
        return "created";
    }

    @GetMapping("/get")
    public List<CustomCollection> getCustomCollection() {
        return mongoService.getCustomCollection();
    }
}
