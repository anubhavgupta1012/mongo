package test.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import test.mongo.entity.CustomCollection;

import java.util.List;

@Service
public class MongoService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public void createEntry(CustomCollection collection) {
        mongoTemplate.save(collection);
    }

    public List<CustomCollection> getCustomCollection() {
        return mongoTemplate.findAll(CustomCollection.class);
    }
}
