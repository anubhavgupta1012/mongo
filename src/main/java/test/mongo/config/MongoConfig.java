package test.mongo.config;

import com.mongodb.ConnectionString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MongoConfig {

    @Value("${spring.data.mongodb.host}")
    private String host;
    @Value("${spring.data.mongodb.port}")
    private String port;
    @Value("${spring.data.mongodb.password}")
    private String password;
    @Value("${spring.data.mongodb.username}")
    private String username;
    @Value("${spring.data.mongodb.database}")
    private String database;


    public MongoDatabaseFactory mongoDbFactory() {
        String connectionString = "mongodb://" + username + ":" + password + "@" + host + ":" + port + "/" + database + "?"
            + "connectTimeoutMS=300&socketTimeoutMS=300&waitQueueTimeoutMS=300&wtimeoutMS=300";

        return new SimpleMongoClientDatabaseFactory(new ConnectionString(connectionString));
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
        System.out.println("template created");
        return mongoTemplate;
    }
}
